package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

public class MainActivity extends Activity {
    EditText etNumber1,etNumber2;
    TextView result;
    double num1,num2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);

    }  //onCreate()

    public void setUpNumbers(){
        try {
            if (etNumber1.getText().toString() == "" || etNumber1.getText().toString() == ""){
                Log.d("CLLCset",etNumber1.getText().toString() + "exe" + etNumber2.getText().toString() );
                Log.d("CLLCset",num1+ "exe//" + num2 );
                throw  new Exception();

            }
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            Log.d("CLLC", "TRY: " + etNumber1.getText().toString() + "exe" + etNumber2.getText().toString() );
            Log.d("CLLC","TRY: " + num1+ "exe//" + num2 );
        }
        catch (Exception e){
            result.setText("Error: Inter a correct number" );
        }
        Log.d("CLLC",etNumber1.getText().toString() +"?" + etNumber2.getText().toString() );
        Log.d("CLLC",num1+ "//" + num2 );
    }


    public void addNums(View v) {
        setUpNumbers();
        double numericResult = num1 + num2;
        result.setText(String.format("%f",numericResult));

    }  //addNums()

    public void subtractNums(View v) {
        setUpNumbers();
        double numericResult = num1 - num2;
        result.setText(String.format("%f",numericResult));

    }  //subtractNums()



    //multiply
    public void multiplyNums(View v) {
        setUpNumbers();
        double numericResult = num1 * num2;
        result.setText(String.format("%f",numericResult));

    }  //multiplyNums()

    //divide
    public void divideNums(View v) {
        setUpNumbers();
        if (num2 == 0){
            result.setText("Can't divide by Zero");
        }else{
            double numericResult = num1 / num2;
            result.setText(String.format("%f",numericResult));
        }
    }  //multiplyNums()

    //cleaar
    public void clear(View v) {
        result.setText("The answer should be here");
        etNumber1.setText("");
        etNumber2.setText("");
        Log.d("CLLC",etNumber1.getText().toString() +"?" + etNumber2.getText().toString() );
        Log.d("CLLC",num1+ "//" + num2 );
        num1 = 0;
        num2 = 0;
    }  //clear()

}